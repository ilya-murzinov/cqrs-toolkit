class: center, middle

# Simple and reliable architecture for backend services

---

# Agenda

- Introduction

- N-tier architecture

- Beyond N-tier architecture

- Demo


---
# N-tier architecture

```
+----------------------+
|                      |
|  API/streams/queues  |
|                      |
+----------------------+
+----------------------+
|                      |
|       Services       |
|                      |
+----------------------+
+----------------------+
|                      |
|     DAO + Models     | // <-----
|                      |
+----------------------+
```

---
# N-tier architecture

```
+----------------------+
|                      |
|  API/streams/queues  |
|                      |
+----------------------+
+----------------------+
|                      |
|       Services       | // <-----
|                      |
+----------------------+
+----------------------+
|                      |
|     DAO + Models     |
|                      |
+----------------------+
```

---
# N-tier architecture

```
+----------------------+
|                      |
|  API/streams/queues  | // <-----
|                      |
+----------------------+
+----------------------+
|                      |
|       Services       |
|                      |
+----------------------+
+----------------------+
|                      |
|     DAO + Models     |
|                      |
+----------------------+
```

---
# Problems

1. Consistency

---

# Money transfer

```java
private static class Account {
    String id;
    int amount;
}

private interface AccountRepo {
    Account get(String accountId);
    void save(Account account);
}
```

---

# Money transfer

```java
private void transfer(String from, String to, int amount) {
    final Account fromAcc = accountRepo.get(from);

    if (fromAcc.amount < amount) throw new RuntimeException("Nope");

    final Account toAcc = accountRepo.get(to);

    fromAcc.amount -= amount;
    toAcc.amount += amount;

    accountRepo.save(fromAcc);
    accountRepo.save(toAcc);
}
```

---

# Money transfer

```java
@Transactional
private void transfer(String from, String to, int amount) {
    final Account fromAcc = accountRepo.get(from);

    if (fromAcc.amount < amount) throw new RuntimeException("Nope");

    final Account toAcc = accountRepo.get(to);

    fromAcc.amount -= amount;
    toAcc.amount += amount;

    accountRepo.save(fromAcc);
    accountRepo.save(toAcc);
}
```

---

# Concurrency control

--

- Pessimistic

--

```sql
BEGIN;
SET TRANSACTION ISOLATION LEVEL `READ COMMITTED`;
SELECT ... FOR UPDATE;
UPDATE ... ;
...
COMMIT;

```

--

```sql
BEGIN;
SET TRANSACTION ISOLATION LEVEL `REPEATABLE READ`;
-- or SET TRANSACTION ISOLATION LEVEL `SERIALIZABLE`;
...
COMMIT;
```

--

- Optimistic

--

```sql
BEGIN;
SET TRANSACTION ISOLATION LEVEL `READ COMMITTED`;
UPDATE ... WHERE version = $1 ... ;
UPDATE ... WHERE updated_date = $1 ... ;
...
COMMIT;
```

---
# Problems

1. Consistency

--

2. Scalability (maintainability)

---
# Scalability

```
+------------+
|    API     |
+------------+
      |
      v
+------------+
|  Service   |
+------------+
      |
      v
+------------+
|    DAO     |
+------------+
```

---

# Scalability

```
+------------+
|    API     |
+------------+
      |
      v
+------------+    +------------+
|  Service   |--->|  Service   |
+------------+    +------------+
      |                 |
      v                 v
+------------+    +------------+
|    DAO     |    |    DAO     |
+------------+    +------------+
```

---

# Proximity design

<img src="proximity-design.png" style="max-width:100%;"/>

---
class: middle, center

# Beyond N-tier architecture

---
class: middle, center

# CQRS & Event-sourcing

---

# Onion architecture

```
+-----------------------------------------------+
|                                               |
|                   Runtime                     |
|                                               |
|    +---------------------------------------+  |
|    |  +---------------+                    |  |
|    |  |               |                    |  |
|    |  |               |                    |  |
|    |  |  Domain Model | Actions (Commands) |  |
|    |  |               |                    |  |
|    |  |               |                    |  |
|    |  +---------------+                    |  |
|    +---------------------------------------+  |
|                                               |
|                   Runtime                     |
|                                               |
+-----------------------------------------------+
```

---

<img src="ddia.png" style="max-width:100%;"/>

---
class: middle, center

# Q&A
