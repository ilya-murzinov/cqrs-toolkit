package core.rest;

import com.fasterxml.jackson.databind.JsonNode;
import core.domain.Model;
import core.domain.Id;
import spark.Request;
import spark.Response;

import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import static core.rest.json.Json.fromJson;
import static core.rest.json.Json.object;
import static core.rest.json.Json.toJson;
import static javax.servlet.http.HttpServletResponse.SC_ACCEPTED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_NOT_MODIFIED;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.eclipse.jetty.http.MimeTypes.Type.APPLICATION_JSON;

public abstract class Resource {

    public abstract void registerRoutes();

    protected static <T> T requestAs(Class<T> type, Request rq) {
        return fromJson(rq.body(), type);
    }

    protected static Object ok(Response rsp) {
        return status(rsp, SC_OK);
    }

    protected static Object ok(Response rsp, JsonNode content) {
        return status(rsp, SC_OK, content);
    }

    protected static Object ok(Response rsp, Object response) {
        return status(rsp, SC_OK, toJson(response));
    }

    protected static Object accepted(Response rsp) {
        return status(rsp, SC_ACCEPTED);
    }

    protected static Object noContent(Response rsp) {
        return status(rsp, SC_NO_CONTENT, "");
    }

    protected static <T> Object noContent(Request rq, Response rs, Class<T> payloadType, Consumer<T> handler) {
        handler.accept(requestAs(payloadType, rq));
        return noContent(rs);
    }

    protected static Object badRequest(Response rs) {
        rs.status(SC_BAD_REQUEST);
        return "";
    }

    protected static Object badRequest(Response rs, JsonNode content) {
        return status(rs, SC_BAD_REQUEST, content);
    }

    protected static Object badRequest(Response rs, String message) {
        return status(rs, SC_BAD_REQUEST, object().put("message", message));
    }

    protected static Object notModified(Response rs) {
        rs.status(SC_NOT_MODIFIED);
        return "";
    }

    protected static Object notFound(Response rs, String message) {
        rs.status(SC_NOT_FOUND);
        rs.type(APPLICATION_JSON.toString());
        return object().put("message", message);
    }

    protected static Object internalServerError(Response rs, String message) {
        rs.status(SC_INTERNAL_SERVER_ERROR);
        return message;
    }

    protected static <T> T status(Response rsp, int status, T content) {
        rsp.status(status);
        rsp.type(APPLICATION_JSON.toString());
        return content;
    }

    protected static Object status(Response rsp, int status) {
        rsp.status(status);
        rsp.type(APPLICATION_JSON.toString());
        return "";
    }

    protected static <T> Object as(Request rq, Class<T> payloadType, Function<T, Object> function) {
        return function.apply(requestAs(payloadType, rq));
    }

    protected static <T> Object as(Request rq, Response rs, Class<T> payloadType, Function<T, Object> function) {
        return function.apply(requestAs(payloadType, rq));
    }

    protected <M extends Model<M, ?, ?>> Id<M> idParam(Request rq, Class<M> type) {
        return idParam(rq, type, "id");
    }

    protected <M extends Model<M, ?, ?>> Id<M> idParam(Request rq, Class<M> type, String paramName) {
        return Id.id(type, idParam(rq, paramName));
    }

    protected UUID idParam(Request rq) {
        return idParam(rq, "id");
    }

    protected UUID idParam(Request rq, String paramName) {
        return UUID.fromString(rq.params(paramName));
    }

}
