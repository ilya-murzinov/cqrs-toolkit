package core.rest.json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_ABSENT;
import static com.fasterxml.jackson.core.JsonGenerator.Feature.WRITE_BIGDECIMAL_AS_PLAIN;
import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static com.fasterxml.jackson.databind.DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS;
import static com.fasterxml.jackson.databind.DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS;
import static com.fasterxml.jackson.databind.SerializationFeature.FAIL_ON_EMPTY_BEANS;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_WITH_ZONE_ID;
import static com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS;

public class Json {

    public static FluentObjectNode object() {
        return new FluentObjectNode(mapper.getNodeFactory());
    }

    public static class FluentObjectNode extends ObjectNode {

        public FluentObjectNode(JsonNodeFactory nc) {
            super(nc);
        }

        public FluentObjectNode put(String fieldName, Optional<?> o) {
            return (FluentObjectNode) o.map(v -> {
                if (v instanceof String) return super.put(fieldName, (String) v);
                if (v instanceof Long) return super.put(fieldName, (Long) v);
                if (v instanceof Integer) return super.put(fieldName, (Integer) v);
                if (v instanceof BigDecimal) return super.put(fieldName, (BigDecimal) v);
                if (v instanceof Boolean) return super.put(fieldName, (Boolean) v);
                if (v instanceof ContainerNode) return super.set(fieldName, (ContainerNode<?>) v);
                else return put(fieldName, v);
            }).orElse(this);
        }

        @Override
        public FluentObjectNode put(String fieldName, String v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, long v) {
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, Long v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, Integer v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, int v) {
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, boolean v) {
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, Boolean v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, BigDecimal v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        @Override
        public FluentObjectNode put(String fieldName, BigInteger v) {
            if (v == null) return this;
            return (FluentObjectNode) super.put(fieldName, v);
        }

        public FluentObjectNode put(String fieldName, Object o) {
            if (o == null) return this;
            return (FluentObjectNode) super.set(fieldName, mapper.valueToTree(o));
        }

        @Override
        public FluentObjectNode deepCopy() {
            final FluentObjectNode ret = new FluentObjectNode(_nodeFactory);

            for (Map.Entry<String, JsonNode> entry : _children.entrySet()) {
                ret._children.put(entry.getKey(), entry.getValue().deepCopy());
            }

            return ret;
        }
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> String toJson(T value) {
        try {
            return mapper.writeValueAsString(value);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static ObjectMapper mapper = new ObjectMapper()
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule())
            .registerModule(new SimpleModule() {{
            }})
            .configure(WRITE_BIGDECIMAL_AS_PLAIN, true)
            .configure(FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(USE_BIG_DECIMAL_FOR_FLOATS, true)
            .configure(WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
            .configure(READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
            .configure(WRITE_DATES_WITH_ZONE_ID, true)
            .configure(FAIL_ON_EMPTY_BEANS, false)
            .setSerializationInclusion(NON_ABSENT);
}
