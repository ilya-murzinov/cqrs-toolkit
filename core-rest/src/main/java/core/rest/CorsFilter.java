package core.rest;

import static spark.Spark.before;
import static spark.Spark.options;

public class CorsFilter {

    public static void enableCORS() {

        options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Request-Method", "POST, GET, OPTIONS, DELETE, PATCH");
            response.header("Access-Control-Allow-Headers", "Content-Type, Cache-Control, Content-Language, Expires, Last-Modified, Pragma, X-Requested-With, Origin, Accept");
        });
    }
}
