package core.test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.join;

public class FluentAssertion {

    public static FluentAssertion that(String description, boolean result) {
        return new FluentAssertion().and(description, result);
    }

    public FluentAssertion and(String description, boolean result) {
        messages.add((result ? "+ " : "- ") + description);
        if (!result) failed = true;
        return this;
    }

    boolean isFailed() {
        return failed;
    }

    String failure() {
        return join("\n\t", messages);
    }

    FluentAssertion() { }

    private final List<String> messages = new ArrayList<>();

    private boolean failed = false;
}
