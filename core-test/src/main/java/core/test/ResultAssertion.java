package core.test;

import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.api.AbstractThrowableAssert;
import core.actions.Error;
import core.actions.Result;

import static org.assertj.core.api.BDDAssertions.then;

public class ResultAssertion<T> extends AbstractObjectAssert<ResultAssertion<T>, Result<T>> {

    public static <T> ResultAssertion<T> assertThat(Result<T> result) {
        return new ResultAssertion<>(result);
    }

    private ResultAssertion(Result<T> actual) {
        super(actual, ResultAssertion.class);
    }

    public AbstractObjectAssert<?, T> isSuccess() {
        try {
            return then(actual.get());
        } catch (Error e) {
            throw new AssertionError("Expected an OK result, but got: " + e);
        }
    }

    @SuppressWarnings("unchecked")
    public AbstractThrowableAssert<?, ? extends Error> isFailure() {
        try {
            actual.get();
            throw new AssertionError("Expected an error result, but got: " + actual.get());
        } catch (Error e) {
            return (AbstractThrowableAssert<?, ? extends Error>) then(e);
        }
    }

    public void isError(Class<? extends Error> errorType) {
        isFailure().isInstanceOf(errorType);
    }
}

