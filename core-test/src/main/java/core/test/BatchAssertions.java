package core.test;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.BDDAssertions;
import core.actions.batch.Batch;
import core.actions.batch.Change;
import core.actions.batch.ChangeType;
import core.domain.Model;
import core.domain.Id;
import core.domain.events.Event;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.stream.Collectors.toList;
import static core.Utils.join;
import static core.actions.batch.ChangeType.DELETE;

public class BatchAssertions extends BDDAssertions {

    public static BatchAssert assertThat(Batch batch) {
        return new BatchAssert(batch);
    }

    public static class BatchAssert extends AbstractAssert<BatchAssert, Batch> {

        private final Set<Change<?>> assertedChanges = new HashSet<>();
        private final Set<Event<?>> assertedEvents = new HashSet<>();

        public BatchAssert(Batch batch) {
            super(batch, BatchAssert.class);
        }

        public <M extends Model<M, ?, ?>> BatchAssert adds(
                Id<M> id,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.ADD, id.toString(), id, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert updates(Id<M> id) {
            return updates(id, m -> FluentAssertion.that("", true));
        }

        public <M extends Model<M, ?, ?>> BatchAssert updates(
                Id<M> id,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.UPDATE, id.toString(), id, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert deletes(Id<M> id) {
            return has(DELETE, id.toString(), id, obj -> new FluentAssertion());
        }

        public <M extends Model<M, ?, ?>> BatchAssert adds(
                String name,
                Id<M> id,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.ADD, name, id, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert updates(
                String name,
                Id<M> id,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.UPDATE, name, id, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert deletes(
                String name,
                Id<M> id,
                Function<M, FluentAssertion> assertions) {
            return has(DELETE, name, id, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert adds(
                Class<? extends M> type,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.ADD, type.getSimpleName(), type, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert updates(
                Class<M> type,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.UPDATE, type.getSimpleName(), type, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert deletes(
                Class<M> type,
                Function<M, FluentAssertion> assertions) {
            return has(DELETE, type.getSimpleName(), type, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert adds(
                String name,
                Class<M> type,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.ADD, name, type, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert updates(
                String name,
                Class<M> type,
                Function<M, FluentAssertion> assertions) {
            return has(ChangeType.UPDATE, name, type, assertions);
        }

        public <M extends Model<M, ?, ?>> BatchAssert deletes(
                String name,
                Class<M> type,
                Function<M, FluentAssertion> assertions) {
            return has(DELETE, name, type, assertions);
        }

        @SuppressWarnings("EqualsBetweenInconvertibleTypes")
        public <M extends Event<?>> BatchAssert hasEvent(
                Class<M> eventType,
                Id<?> modelId,
                Function<M, FluentAssertion> eventAssertions) {
            return hasEvent(eventType, modelId.type, e -> e.modelId.equals(modelId), eventAssertions);
        }

        public <M extends Event<?>> BatchAssert hasEvent(
                Class<M> eventType,
                Class<? extends Model<?, ?, ?>> modelType,
                Function<M, FluentAssertion> eventAssertions) {
            return hasEvent(eventType, modelType, e -> true, eventAssertions);
        }

        public void only() {
            final Collection<Change<?>> unassertedChanges = actual.changes()
                    .stream()
                    .filter(change -> !assertedChanges.contains(change))
                    .collect(toList());

            final Collection<Event<?>> unassertedEvents = actual.events()
                    .stream()
                    .filter(event -> !assertedEvents.contains(event))
                    .collect(toList());

            final StringBuilder message = new StringBuilder();

            if (!unassertedChanges.isEmpty()) {
                message.append("Batch should not contain more changes, but contains:\n");
                message.append(join("\n\t", unassertedChanges));
                message.append("\n\t");
            }

            if (!unassertedEvents.isEmpty()) {
                message.append("Batch should not contain more events, but contains:\n");
                message.append(join("\n\t", unassertedEvents));
                message.append("\n\t");
            }


            if (message.length() > 0)
                throw new AssertionError(message.toString());
        }

        public void isEmpty() {
            if (actual.changes().size() > 0 || actual.events().size() > 0)
                throw new AssertionError("Expected an empty batch, but got " + actual);
        }

        private <M extends Model<M, ?, ?>> BatchAssert has(
                ChangeType changeType,
                String name,
                Class<? extends M> type,
                Function<M, FluentAssertion> assertions) {
            return has(change -> true, changeType, name, type, assertions);
        }

        private <M extends Model<M, ?, ?>> BatchAssert has(
                ChangeType changeType,
                String name,
                Id<? extends M> id,
                Function<M, FluentAssertion> assertions) {
            return has(change -> id.equals(change.id), changeType, name, id.type, assertions);
        }

        private <M extends Model<M, ?, ?>> BatchAssert has(
                Predicate<Change<?>> predicate,
                ChangeType changeType,
                String name,
                Class<? extends M> type,
                Function<M, FluentAssertion> assertions) {
            isNotNull();

            final Optional<FluentAssertion> assertion = actual.changes()
                    .stream()
                    .filter(change -> change.changeType == changeType)
                    .filter(predicate)
                    .filter(change -> change.model.map(type::isInstance).orElse(true))
                    .findFirst()
                    .map(this::recordAssertedChange)
                    .flatMap(change -> change.model.map(e -> assertions.apply(type.cast(e))));

            final Optional<FluentAssertion> failedAssertion = assertion.filter(FluentAssertion::isFailed);

            if (!assertion.isPresent())
                failWithMessage("Batch should contain %s for %s", changeType, name);
            else
                failedAssertion.ifPresent(fluentAssertion -> failWithMessage(
                    "Expected batch [%s]\nto contain %s for %s that...\n%s",
                    actual,
                    changeType,
                    name,
                    fluentAssertion.failure()));

            return this;
        }

        @SuppressWarnings("unchecked")
        private <EV extends Event<?>, M extends Model<?, ?, ?>> BatchAssert hasEvent(
                Class<EV> eventType,
                Class<M> modelType,
                Predicate<EV> predicate,
                Function<EV, FluentAssertion> eventAssertions) {
            isNotNull();

            isNotNull();

            final Optional<FluentAssertion> assertion = actual.events()
                    .stream()
                    .filter(eventType::isInstance)
                    .filter(event -> event.modelId.type == modelType)
                    .map(eventType::cast)
                    .filter(predicate)
                    .findFirst()
                    .map(this::recordAssertedEvent)
                    .map(e -> eventAssertions.apply((EV) e));

            final Optional<FluentAssertion> failedAssertion = assertion.filter(FluentAssertion::isFailed);

            if (!assertion.isPresent())
                failWithMessage("Batch should contain %s for %s", eventType, modelType);
            else
                failedAssertion.ifPresent(fluentAssertion -> failWithMessage(
                    "Expected batch [%s]\nto contain %s for %s that...\n%s",
                    actual,
                    eventType,
                    modelType,
                    fluentAssertion.failure()));

            return this;
        }

        private Change<?> recordAssertedChange(Change<?> change) {
            assertedChanges.add(change);
            return change;
        }

        private Event<?> recordAssertedEvent(Event<?> event) {
            assertedEvents.add(event);
            return event;
        }
    }
}
