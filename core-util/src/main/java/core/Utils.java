package core;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class Utils {
    private Utils() {
        // nope
    }

    public static <T> Optional<T> optional(T value) {
        return Optional.ofNullable(value);
    }

    public static <T> Optional<T> some(T value) {
        return Optional.of(value);
    }

    public static <T> Optional<T> none() {
        return Optional.empty();
    }

    public static void checkState(boolean condition, String errorMessageTemplate, Object... params) {
        if (!condition)
            throw new IllegalStateException(format(errorMessageTemplate, params));
    }

    public static String join(String delimiter, Collection<?> objects) {
        return objects.stream().map(Objects::toString).collect(joining("\n\t"));
    }

}
