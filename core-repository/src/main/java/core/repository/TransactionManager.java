package core.repository;

import lombok.extern.slf4j.Slf4j;
import org.jooq.ConnectionProvider;

import java.sql.Connection;
import java.util.concurrent.Callable;

import static java.sql.Connection.TRANSACTION_READ_COMMITTED;

@Slf4j
public class TransactionManager {
    private final ConnectionProvider connectionProvider;

    public TransactionManager(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public void inTransaction(Runnable runnable) {
        inTransaction(() -> {
            runnable.run();
            return null;
        });
    }

    public <T> void inTransaction(Callable<T> callable) {
        final Connection connection = connectionProvider.acquire();
        try {
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(TRANSACTION_READ_COMMITTED);
            callable.call();
            connection.commit();
        } catch (OptimisticLockingException e) {
            rollback(connection, e);
            throw e;
        } catch (Exception e) {
            rollback(connection, e);
            throw new RuntimeException(e);
        } finally {
            close(connection);
        }
    }

    private void rollback(Connection connection, Exception original) {
        try {
            connection.rollback();
        } catch (Exception e) {
            log.error("Couldn't rollback due to {}, original exception is {}", original, e);
        }
    }

    private void close(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            log.error("Couldn't close connection due to {}", e);
        }
    }
}
