package core.repository;

public class DbConfig {
    public final String connectionString;
    public final String user;
    public final String password;

    public DbConfig(String connectionString, String user, String password) {
        this.connectionString = connectionString;
        this.user = user;
        this.password = password;
    }
}
