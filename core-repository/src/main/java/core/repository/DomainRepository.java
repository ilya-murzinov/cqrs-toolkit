package core.repository;

import core.domain.Id;
import core.domain.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;

@SuppressWarnings("unchecked")
public class DomainRepository {

    private final Map<Class<? extends Model<?, ?, ?>>, ModelRepository> repositories;

    private DomainRepository(Builder builder) {
        this.repositories = Map.copyOf(builder.repositories);
    }

    public <M extends Model<?, ?, ?>> Optional<M> find(Id<M> id) {
        return (Optional<M>) repositoryFor(id.type).find(id);
    }

    public <M extends Model<?, ?, ?>> M get(Id<M> id) {
        return (M) repositoryFor(id.type).get(id);
    }

    public <M extends Model<?, ?, ?>> M add(M model) {
        return (M) repositoryFor(classOf(model)).add(model);
    }

    public <M extends Model<?, ?, ?>> M update(M model) {
        return (M) repositoryFor(classOf(model)).update(model);
    }

    public <M extends Model<?, ?, ?>> boolean delete(Id<M> id) {
        return repositoryFor(id.type).delete(id);
    }

    @SuppressWarnings("unchecked")
    private static <M extends Model<?, ?, ?>> Class<M> classOf(M model) {
        return (Class<M>) model.getClass();
    }

    private ModelRepository repositoryFor(Class<? extends Model<?, ?, ?>> modelClass) {
        final ModelRepository found = repositories.get(modelClass);

        if (found == null)
            throw new RuntimeException(format("Repository for %s not found", modelClass));

        return found;
    }

    public static class Builder {

        private final Map<Class<? extends Model<?, ?, ?>>, ModelRepository<?, ?>> repositories = new HashMap<>();

        private Builder() {

        }

        public static Builder domainRepository() {
            return new Builder();
        }

        public Builder addRepository(Class<? extends Model<?, ?, ?>> modelClass,
                                     ModelRepository<?, ?> repo) {
            repositories.put(modelClass, repo);
            return this;
        }

        public DomainRepository build() {
            return new DomainRepository(this);
        }
    }
}
