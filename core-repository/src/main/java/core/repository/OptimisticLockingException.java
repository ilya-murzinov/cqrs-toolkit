package core.repository;

import core.domain.Model;

import static java.lang.String.format;

public class OptimisticLockingException extends RuntimeException {

    public OptimisticLockingException(Model model) {
        super(format("Couldn't update stale model [%s]", model));
    }
}
