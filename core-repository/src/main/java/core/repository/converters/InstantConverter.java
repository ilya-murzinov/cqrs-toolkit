package core.repository.converters;

import org.jooq.Converter;

import java.sql.Timestamp;
import java.time.Instant;

public class InstantConverter implements Converter<Timestamp, Instant> {

    public static final InstantConverter INSTANCE = new InstantConverter();

    @Override
    public Instant from(Timestamp timestamp) {
        return timestamp != null ? timestamp.toInstant() : null;
    }

    @Override
    public Timestamp to(Instant instant) {
        return instant != null ? Timestamp.from(instant) : null;
    }

    @Override
    public Class<Timestamp> fromType() {
        return Timestamp.class;
    }

    @Override
    public Class<Instant> toType() {
        return Instant.class;
    }
}