package core.repository;

import core.domain.Id;
import core.domain.Model;
import org.jooq.Condition;
import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Table;
import org.jooq.TableRecord;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static core.domain.State.GenericState.DELETED;
import static core.repository.NotFoundException.notFound;
import static java.lang.String.format;
import static java.time.Instant.now;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.RenderNameStyle.LOWER;

public abstract class ModelRepository<M extends Model<M, ?, ?>, R extends Record> {

    private static final String ID = "id";
    private static final String CREATED_DATE = "created_date";
    private static final String UPDATED_DATE = "updated_date";
    private static final String STATE = "state";

    private static final Settings SETTINGS = new Settings();

    static {
        SETTINGS.setRenderNameStyle(LOWER);
        SETTINGS.setRenderSchema(false);
        SETTINGS.setExecuteLogging(false);
        SETTINGS.setAttachRecords(false);
        SETTINGS.setUpdatablePrimaryKeys(false);
    }

    private final Class<R> record;
    private final Class<M> modelType;
    private final Table<? extends TableRecord> table;
    private final Field<UUID> idField;
    private final Field<Instant> createdDateField;
    private final Field<Instant> updatedDateField;
    private final Field<String> stateField;

    protected final DSLContext db;
    protected final DSLContext readonlyDb;

    protected ModelRepository(Class<R> record,
                              Class<M> modelType,
                              Table<? extends TableRecord> table,
                              ConnectionProvider connectionProvider) {
        this(record, modelType, table, connectionProvider, connectionProvider);
    }

    protected ModelRepository(Class<R> record,
                              Class<M> modelType,
                              Table<? extends TableRecord> table,
                              ConnectionProvider writeConnectionProvider,
                              ConnectionProvider readConnectionProvider) {
        this.table = table;
        this.modelType = modelType;
        this.idField = fieldByName(table, ID, UUID.class);
        this.createdDateField = fieldByName(table, CREATED_DATE, Instant.class);
        this.updatedDateField = fieldByName(table, UPDATED_DATE, Instant.class);
        this.stateField = fieldByName(table, STATE, String.class);
        this.record = record;

        this.db = DSL.using(new DefaultConfiguration()
                .set(POSTGRES)
                .set(writeConnectionProvider)
                .set(SETTINGS));

        this.readonlyDb = DSL.using(new DefaultConfiguration()
                .set(POSTGRES)
                .set(readConnectionProvider)
                .set(SETTINGS));
    }

    public Optional<M> find(Id<M> id) {
        return findOneWhere(idField.eq(id.value));
    }

    public Optional<M> findOneWhere(Condition condition) {
        return findAllWhere(condition).stream().findFirst();
    }

    public List<M> findAllWhere(Condition condition) {
        return db.select(table.fields())
                .from(table)
                .where(condition.and(notDeleted()))
                .fetchInto(record)
                .stream()
                .map(this::fromRecord)
                .collect(toList());
    }

    public M get(Id<M> id) {
        return find(id).orElseThrow(() -> notFound(id));
    }

    public M add(M model) {
        db.executeInsert((TableRecord) toRecord(model));
        return model;
    }

    public M update(M model) {
        final TableRecord record = (TableRecord) toRecord(model);
        record.set(updatedDateField, now());

        final int updated = db.update(table)
                .set(record)
                .where(idField.eq(model.id).and(updatedDateField.eq(model.updated)))
                .execute();

        if (updated == 0)
            throw new OptimisticLockingException(model);

        return model;
    }

    public boolean delete(Id<M> id) {
        return db.update(table).set(stateField, DELETED.name()).where(idField.eq(id.value)).execute() == 1;
    }

    protected abstract M fromRecord(R record);

    protected abstract R toRecord(M model);

    protected Condition notDeleted() {
        return stateField.ne(DELETED.name());
    }

    private static <T> Field<T> fieldByName(Table<? extends TableRecord> table, String fieldName, Class<T> type) {
        return stream(table.fields())
                .filter(f -> f.getName().equalsIgnoreCase(fieldName))
                .map(f -> f.coerce(type))
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException(format("Field %s not found in table %s", fieldName, table)));
    }
}
