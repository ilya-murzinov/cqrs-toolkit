package core.repository;

import org.jooq.ConnectionProvider;
import org.jooq.impl.DataSourceConnectionProvider;
import org.postgresql.ds.PGSimpleDataSource;

public class SimpleConnectionProvider {

    public static ConnectionProvider simpleConnectionProvider(DbConfig config) {
        final PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setUrl(config.connectionString);
        ds.setUser(config.user);
        ds.setPassword(config.password);
        return new DataSourceConnectionProvider(ds);
    }
}
