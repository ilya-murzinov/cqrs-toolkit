package core.repository;

import core.domain.Id;

import static java.lang.String.format;

public class NotFoundException extends RuntimeException {

    public NotFoundException(Id<?> id) {
        super(format("%s not found", id));
    }

    public static NotFoundException notFound(Id<?> id) {
        return new NotFoundException(id);
    }
}
