package core.domain;

import static java.util.Arrays.asList;

public interface State {

    String name();

    default <S> boolean in(S... states) {
        return asList(states).contains(this);
    }

    enum GenericState {
        DELETED
    }
}
