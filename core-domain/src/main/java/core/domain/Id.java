package core.domain;

import lombok.EqualsAndHashCode;

import java.util.UUID;

@EqualsAndHashCode
public class Id<M extends Model<?, ?, ?>> {
    public final UUID value;
    public final Class<M> type;

    private Id(UUID value, Class<M> type) {
        this.value = value;
        this.type = type;
    }

    public static <M extends Model<?, ?, ?>> Id<M> id(Class<M> type, UUID id) {
        return new Id<>(id, type);
    }

    @Override
    public String toString() {
        return type.getSimpleName() + "[" + value + "]";
    }

}
