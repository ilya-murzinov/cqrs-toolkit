package core.domain.events;

import core.domain.Model;
import core.domain.Id;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ModelDeletedEvent<M extends Model<?, ?, ?>> extends Event<M> {

    public ModelDeletedEvent(Id<M> modelId) {
        super(modelId);
    }
}
