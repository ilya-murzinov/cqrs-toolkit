package core.domain.events;

import core.domain.Model;
import core.domain.Id;
import core.domain.State;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ModelStateChangedEvent<M extends Model<M, ?, ?>, S extends State> extends Event<M> {

    public final S oldState;
    public final S newState;

    public ModelStateChangedEvent(Id<M> modelId, S oldState, S newState) {
        super(modelId);
        this.oldState = oldState;
        this.newState = newState;
    }

    public boolean hasTransition(S oldState, S newState) {
        return this.oldState.equals(oldState) && this.newState.equals(newState);
    }
}
