package core.domain.events;

import core.domain.Model;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ModelCreatedEvent<M extends Model<M, ?, ?>> extends Event<M> {

    public final M model;

    public ModelCreatedEvent(M model) {
        super(model.id());
        this.model = model;
    }
}
