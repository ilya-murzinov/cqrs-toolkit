package core.domain.events;

import core.domain.Model;
import core.domain.Id;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.Instant;

import static java.time.Instant.now;

@EqualsAndHashCode
@ToString
public abstract class Event<M extends Model<?, ?, ?>> {

    public final Id<M> modelId;
    public final Instant eventDate;

    protected Event(Id<M> modelId) {
        this.modelId = modelId;
        this.eventDate = now();
    }
}
