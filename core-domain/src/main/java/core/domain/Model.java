package core.domain;

import core.domain.events.Event;
import core.domain.events.ModelStateChangedEvent;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static core.Utils.checkState;

@EqualsAndHashCode(exclude = {"events", "updated"})
@ToString
public abstract class Model<
        M extends Model<M, S, B>,
        S extends State,
        B extends ModelBuilder<M, S, B>> {

    public final UUID id;
    public final S state;
    public final Instant created;
    public final Instant updated;

    private final List<Event<M>> events;

    protected Model(ModelBuilder<M, S, B> builder) {
        this.id = builder.id;
        this.state = builder.state;
        this.created = builder.created;
        this.updated = builder.updated != null ? builder.updated : builder.created;
        this.events = builder.events;
    }

    @SuppressWarnings("unchecked")
    public final Id<M> id() {
        return Id.id((Class<M>) getClass(), id);
    }

    protected abstract B copy();

    public M withState(S state) {
        return copy()
                .state(state)
                .addEvent(new ModelStateChangedEvent<>(this.id(), this.state, state))
                .build();
    }

    public final List<Event<M>> events() {
        return List.copyOf(events);
    }

    public boolean is(S state) {
        return this.state == state;
    }

    protected void checkStateIs(S from) {
        checkState(is(from), "Model %s must be %s, but is %s", id, from, state);
    }
}
