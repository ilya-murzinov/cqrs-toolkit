package core.domain;

import core.domain.events.Event;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.time.Instant.now;

@SuppressWarnings("unchecked")
public abstract class ModelBuilder<
        M extends Model<M, S, B>,
        S extends State,
        B extends ModelBuilder<M, S, B>> {

    protected UUID id;

    protected S state;
    protected Instant created = now();
    protected Instant updated;

    protected List<Event<M>> events = new ArrayList<>();

    private B id(UUID id) {
        this.id = id;
        return (B) this;
    }

    protected B existing(UUID id) {
        return id(id);
    }

    protected B newModel() {
        id(UUID.randomUUID());
        return (B) this;
    }

    public B created(Instant created) {
        this.created = created;
        return (B) this;
    }

    public B updated(Instant updated) {
        this.updated = updated;
        return (B) this;
    }

    public B baseCopyOf(Model<M, S, B> model) {
        return id(model.id)
                .created(model.created)
                .updated(model.updated)
                .state(model.state);
    }

    public B state(S state) {
        this.state = state;
        return (B) this;
    }

    public B addEvent(Event<M> event) {
        this.events.add(event);
        return (B) this;
    }

    public abstract M build();
}
