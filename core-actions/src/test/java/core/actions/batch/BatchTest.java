package core.actions.batch;

import org.junit.jupiter.api.Test;
import core.domain.Model;
import core.domain.ModelBuilder;
import core.domain.State;
import core.domain.events.ModelCreatedEvent;
import core.domain.events.ModelStateChangedEvent;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static core.actions.batch.BatchTest.Dummy.Builder.dummy;
import static core.actions.batch.BatchTest.Dummy.DummyState.ACTIVE;
import static core.actions.batch.BatchTest.Dummy.DummyState.INACTIVE;

class BatchTest {

    @Test
    void should_add_new_model() {
        // given
        final Batch batch = Batch.emptyBatch();
        final Dummy dummy = dummy().build();

        // when
        batch.add(dummy);

        // then
        assertThat(batch.events()).hasSize(1);
        assertThat(batch.changes()).hasSize(1);

        assertThat(batch.events()).hasOnlyOneElementSatisfying(e -> {
            final Dummy model = (Dummy) ((ModelCreatedEvent) e).model;
            assertThat(model).isEqualTo(dummy);
        });
        assertThat(batch.changes()).contains(new Batch.Add<>(dummy));
    }

    @Test
    void should_update_model() {
        // given
        final Batch batch = Batch.emptyBatch();
        final Dummy dummy = dummy().build();
        final Dummy deactivated = dummy.deactivate();

        // when
        batch.update(deactivated);

        // then
        assertThat(batch.events()).hasOnlyOneElementSatisfying(e -> {
            final ModelStateChangedEvent stateChangedEvent = (ModelStateChangedEvent) e;
            assertThat(stateChangedEvent.hasTransition(ACTIVE, INACTIVE)).isTrue();
        });
        assertThat(batch.changes()).contains(new Batch.Update<>(deactivated));
    }

    @Test
    void should_delete_model() {
        // given
        final Batch batch = Batch.emptyBatch();
        final Dummy dummy = dummy().build();

        // when
        batch.delete(dummy);

        // then
        assertThat(batch.events())
                .hasOnlyOneElementSatisfying(e -> assertThat(e.modelId).isEqualTo(dummy.id()));
        assertThat(batch.changes()).contains(new Batch.Delete<>(dummy.id()));
    }

    @Test
    void should_throw_error__when_change_for_model_is_already_tracked() {
        // given
        final Batch batch = Batch.emptyBatch();
        final Dummy dummy = dummy().build();
        batch.update(dummy);

        // then
        assertThatThrownBy(() -> batch.add(dummy))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Model Dummy[%s] already tracked for UPDATE", dummy.id);
    }

    public static class Dummy extends Model<Dummy, Dummy.DummyState, Dummy.Builder> {

        protected Dummy(Builder builder) {
            super(builder);
        }

        public Dummy deactivate() {
            checkStateIs(ACTIVE);
            return withState(INACTIVE);
        }

        @Override
        public Builder copy() {
            return new Builder().baseCopyOf(this);
        }

        static class Builder extends ModelBuilder<Dummy, DummyState, Builder> {

            private Builder() {
                newModel().state(ACTIVE);
            }

            public static Builder dummy() {
                return new Builder();
            }

            @Override
            public Dummy build() {
                return new Dummy(this);
            }
        }

        public enum DummyState implements State {
            ACTIVE,
            INACTIVE
        }
    }
}
