package core.actions.events;

import core.domain.events.Event;

public interface EventHandler {

    void handle(Event<?> event);
}
