package core.actions.batch;

public enum ChangeType {
    ADD,
    UPDATE,
    DELETE
}
