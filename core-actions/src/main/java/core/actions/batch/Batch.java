package core.actions.batch;

import core.domain.Id;
import core.domain.Model;
import core.domain.events.Event;
import core.domain.events.ModelCreatedEvent;
import core.domain.events.ModelDeletedEvent;
import lombok.ToString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static core.Utils.none;
import static core.Utils.some;
import static core.actions.batch.ChangeType.ADD;
import static core.actions.batch.ChangeType.DELETE;
import static core.actions.batch.ChangeType.UPDATE;
import static java.lang.String.format;

@ToString
public class Batch {
    private final Map<Id<?>, Change<?>> changes;
    private final List<Event<?>> events;

    private Batch() {
        this.changes = new HashMap<>();
        this.events = new ArrayList<>();
    }

    public static Batch emptyBatch() {
        return new Batch();
    }

    public List<Change<?>> changes() {
        return List.copyOf(new ArrayList<>(changes.values()));
    }

    public List<Event<?>> events() {
        return List.copyOf(events);
    }

    public <M extends Model<M, ?, ?>> void add(M model) {
        checkUnique(model.id());
        changes.put(model.id(), new Add<>(model));
        events.addAll(model.events());
        events.add(new ModelCreatedEvent<>(model));
    }

    public <M extends Model<M, ?, ?>> void update(M model) {
        checkUnique(model.id());
        changes.put(model.id(), new Update<>(model));
        events.addAll(model.events());
    }

    public <M extends Model<M, ?, ?>> void delete(M model) {
        checkUnique(model.id());
        changes.put(model.id(), new Delete<>(model.id()));
        events.addAll(model.events());
        events.add(new ModelDeletedEvent<>(model.id()));
    }

    private <M extends Model<M, ?, ?>> void checkUnique(Id<M> id) {
        final Change<?> tracked = changes.get(id);
        if (tracked != null)
            throw new IllegalStateException(format("Model %s already tracked for %s", id, tracked.changeType));
    }

    @ToString(callSuper = true)
    static class Add<M extends Model<M, ?, ?>> extends Change<M> {

        Add(M model) {
            super(ADD, model.id(), some(model));
        }
    }

    @ToString(callSuper = true)
    static class Update<M extends Model<M, ?, ?>> extends Change<M> {

        Update(M model) {
            super(UPDATE, model.id(), some(model));
        }
    }

    @ToString(callSuper = true)
    static class Delete<M extends Model<M, ?, ?>> extends Change<M> {

        Delete(Id<M> id) {
            super(DELETE, id, none());
        }
    }
}
