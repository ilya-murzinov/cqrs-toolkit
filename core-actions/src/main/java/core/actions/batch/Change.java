package core.actions.batch;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import core.domain.Model;
import core.domain.Id;

import java.util.Optional;

@EqualsAndHashCode
@ToString
public abstract class Change<M extends Model<M, ?, ?>> {

    public final ChangeType changeType;
    public final Id<M> id;
    public final Optional<M> model;

    protected Change(ChangeType changeType, Id<M> id, Optional<M> model) {
        this.changeType = changeType;
        this.id = id;
        this.model = model;
    }
}
