package core.actions;

import lombok.ToString;

public abstract class Result<T> {

    public abstract T get();

    public static <T> Result<T> ok(T value) {
        return new Success<>(value);
    }

    public static <T> Result<T> error(Error error) {
        return new Failure<>(error);
    }

    @ToString
    public static class Success<T> extends Result<T> {
        final T value;

        private Success(T value) {
            this.value = value;
        }

        @Override
        public T get() {
            return value;
        }
    }

    @ToString
    public static class Failure<T> extends Result<T> {
        final Error error;

        private Failure(Error error) {
            this.error = error;
        }

        @Override
        public T get() {
            throw new Error("Nope");
        }
    }
}
