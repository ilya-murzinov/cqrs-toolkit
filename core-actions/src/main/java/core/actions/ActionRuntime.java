package core.actions;

import core.actions.batch.Batch;
import core.actions.batch.Change;
import core.actions.events.EventHandler;
import core.repository.DomainRepository;
import core.repository.OptimisticLockingException;
import core.repository.TransactionManager;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static core.actions.ActionRuntime.Config.defaultConfig;
import static java.time.Instant.now;
import static java.util.UUID.randomUUID;

@Slf4j
public class ActionRuntime {
    private final ActionsRegistry registry;
    private final DomainRepository domainRepo;
    private final TransactionManager tx;
    private final EventHandler eventHandler;

    public ActionRuntime(ActionsRegistry registry,
                         DomainRepository domainRepo,
                         TransactionManager tx,
                         EventHandler eventHandler) {
        this.registry = registry;
        this.domainRepo = domainRepo;
        this.tx = tx;
        this.eventHandler = eventHandler;
    }

    public <A extends Action<IN, OUT>, IN, OUT> Result<OUT> executeRetriable(Class<A> actionClass, IN params) {
        return execute(actionClass, params, defaultConfig());
    }

    public <A extends Action<IN, OUT>, IN, OUT> Result<OUT> execute(Class<A> actionClass,
                                                                    IN params,
                                                                    Config config) {
        final Instant stop = now().plus(config.tolerance);
        final UUID actionId = randomUUID();
        final String actionName = actionClass.getSimpleName() + "#" + actionId;

        log.info("Running action {} with parameters {}", actionName, params);

        A action = registry.createAction(actionClass);
        Result<OUT> result = action.run(params);

        while (true) {
            try {
                final Batch batch = action.batch;
                log.info("Persisting batch {} returned by action {}", batch, actionName);

                tx.inTransaction(() -> {
                    persist(batch.changes());
                    batch.events().forEach(eventHandler::handle);
                });

                log.info("Action {} finished with result {}", actionName, result);

                return result;
            } catch (OptimisticLockingException e) {
                log.info("Got {} when executing {}, retrying", e, actionName);

                if (now().isBefore(stop)) {
                    action = registry.createAction(actionClass);
                    result = action.run(params);
                } else
                    throw e;
            }
        }
    }

    private void persist(List<Change<?>> changes) {
        changes.forEach(change -> {
            switch (change.changeType) {
                case ADD:
                    domainRepo.add(change.model.get());
                    break;
                case UPDATE:
                    domainRepo.update(change.model.get());
                    break;
                case DELETE:
                    domainRepo.delete(change.id);
                    break;
                default:
                    throw new RuntimeException("Oops!");
            }
        });
    }

    @AllArgsConstructor
    public static class Config {
        private static final Duration DEFAULT_TOLERANCE = Duration.ofMillis(500);

        public final boolean retriable;
        public final Duration tolerance;

        public static Config defaultConfig() {
            return new Config(true, DEFAULT_TOLERANCE);
        }

        public static Config nonRetriable() {
            return new Config(false, DEFAULT_TOLERANCE);
        }
    }
}
