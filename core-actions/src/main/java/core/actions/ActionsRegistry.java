package core.actions;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class ActionsRegistry {
    private final Map<Class<? extends Action<?, ?>>, Supplier<? extends Action<?, ?>>> registry = new HashMap<>();

    public static ActionsRegistry actionsRegistry() {
        return new ActionsRegistry();
    }

    public <A extends Action<?, ?>> ActionsRegistry register(Class<A> actionClass, Supplier<A> creator) {
        registry.put(actionClass, creator);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <A extends Action<?, ?>> A createAction(Class<A> actionClass) {
        return (A) registry.get(actionClass).get();
    }
}
