package core.actions;

public class Error extends RuntimeException {

    public Error(String message) {
        super(message);
    }
}
