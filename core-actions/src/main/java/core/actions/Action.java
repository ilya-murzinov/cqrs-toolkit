package core.actions;

import core.actions.batch.Batch;

public abstract class Action<IN, OUT> {

    public final Batch batch = Batch.emptyBatch();

    public abstract Result<OUT> run(IN params);
}
