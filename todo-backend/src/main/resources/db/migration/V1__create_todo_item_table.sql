CREATE TABLE todo_item (
    id UUID PRIMARY KEY,
    state VARCHAR(25) NOT NULL,
    title TEXT NOT NULL,
    description TEXT NULL,
    created_date TIMESTAMP NOT NULL,
    updated_date TIMESTAMP NOT NULL
);