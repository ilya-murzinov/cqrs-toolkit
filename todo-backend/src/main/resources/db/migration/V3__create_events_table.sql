CREATE TABLE event_log (
    id UUID PRIMARY KEY,
    model_id UUID,
    data TEXT NULL,
    event_date TIMESTAMP NOT NULL
);