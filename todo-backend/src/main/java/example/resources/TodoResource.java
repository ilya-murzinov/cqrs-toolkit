package example.resources;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ObjectNode;
import core.actions.ActionRuntime;
import core.actions.Result;
import core.domain.Id;
import core.rest.Resource;
import example.actions.TodoItemCompleteAction;
import example.actions.TodoItemCreateAction;
import example.actions.TodoItemDeleteAction;
import example.actions.TodoItemUpdateAction;
import example.domain.TodoItem;
import example.repository.TodoItemRepository;
import spark.Route;

import java.util.Optional;

import static core.rest.json.Json.object;
import static example.domain.TodoItem.TodoItemState.DONE;
import static java.util.stream.Collectors.toList;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.patch;
import static spark.Spark.path;
import static spark.Spark.post;

public class TodoResource extends Resource {

    private final TodoItemRepository todoItemRepo;
    private final ActionRuntime actionRuntime;
    private final String externalUrl;

    private final Routes routes;

    public TodoResource(TodoItemRepository todoItemRepo,
                        ActionRuntime actionRuntime,
                        String externalUrl) {
        this.todoItemRepo = todoItemRepo;
        this.actionRuntime = actionRuntime;
        this.externalUrl = externalUrl;
        this.routes = new Routes();
    }

    @Override
    public void registerRoutes() {
        path("/todos", () -> {
            get("/", routes.getTodos);
            get("/:id", routes.getTodo);

            post("/", routes.createTodo);
            patch("/:id", routes.updateTodo);
            delete("/:id", routes.deleteTodo);
        });
    }

    private class Routes {
        private Route getTodos = (rq, rs) ->
                ok(rs, todoItemRepo.findAll()
                    .stream()
                    .map(this::toJson)
                    .collect(toList()));

        private Route getTodo = (rq, rs) ->
                ok(rs, toJson(todoItemRepo.get(idParam(rq, TodoItem.class))));

        private Route createTodo = (rq, rs) -> as(rq, CreateTodoRequest.class, request -> {
            final Result<TodoItem> created = actionRuntime.executeRetriable(
                    TodoItemCreateAction.class,
                    new TodoItemCreateAction.Params(request.title, request.order, request.description));

            return ok(rs, toJson(created.get()));
        });

        private Route updateTodo = (rq, rs) -> as(rq, UpdateTodoRequest.class, request -> {
            final Id<TodoItem> id = idParam(rq, TodoItem.class);

            final Result<TodoItem> updated = request.completed.filter(c -> c)
                    .map(__ -> actionRuntime.executeRetriable(
                            TodoItemCompleteAction.class,
                            new TodoItemCompleteAction.Params(id, request.title)))
                    .orElseGet(() -> actionRuntime.executeRetriable(
                            TodoItemUpdateAction.class,
                            new TodoItemUpdateAction.Params(
                                    id,
                                    request.title,
                                    request.order,
                                    request.description)));

            return ok(rs, toJson(updated.get()));
        });

        private Route deleteTodo = (rq, rs) -> {
            final Result<TodoItem> deleted = actionRuntime.executeRetriable(
                    TodoItemDeleteAction.class,
                    new TodoItemDeleteAction.Params(idParam(rq, TodoItem.class)));

            return ok(rs, toJson(deleted.get()));
        };

        private ObjectNode toJson(TodoItem todoItem) {
            return object()
                    .put("id", todoItem.id)
                    .put("title", todoItem.title)
                    .put("order", todoItem.order)
                    .put("completed", todoItem.is(DONE))
                    .put("url", externalUrl + "/" + todoItem.id);
        }
    }

    private static class CreateTodoRequest {
        public final String title;
        public final int order;
        public final Optional<String> description;

        @JsonCreator
        private CreateTodoRequest(@JsonProperty("title") String title,
                                  @JsonProperty("order") int order,
                                  @JsonProperty("description") Optional<String> description) {
            this.title = title;
            this.order = order;
            this.description = description;
        }
    }

    private static class UpdateTodoRequest {
        public final Optional<String> title;
        public final Optional<Integer> order;
        public final Optional<String> description;
        public final Optional<Boolean> completed;

        @JsonCreator
        private UpdateTodoRequest(@JsonProperty("title") Optional<String> title,
                                  @JsonProperty("order") Optional<Integer> order,
                                  @JsonProperty("description") Optional<String> description,
                                  @JsonProperty("completed") Optional<Boolean> completed) {
            this.title = title;
            this.order = order;
            this.description = description;
            this.completed = completed;
        }
    }
}
