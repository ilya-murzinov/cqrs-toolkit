package example.resources;

import core.rest.Resource;
import example.domain.TodoItem;
import example.repository.TodoItemRepository;
import org.jooq.impl.DSL;
import spark.Route;

import java.util.List;

import static spark.Spark.delete;

public class InternalTodoResource extends Resource {

    private final TodoItemRepository todoItemRepo;

    private final Routes routes;

    public InternalTodoResource(TodoItemRepository todoItemRepo) {
        this.todoItemRepo = todoItemRepo;
        this.routes = new Routes();
    }

    @Override
    public void registerRoutes() {
        delete("/todos/", routes.deleteTodo);
    }

    private class Routes {
        private Route deleteTodo = (rq, rs) -> {
            final List<TodoItem> allItems = todoItemRepo.findAllWhere(DSL.trueCondition());
            allItems.stream().map(TodoItem::id).forEach(todoItemRepo::delete);
            return ok(rs, allItems);
        };
    }
}
