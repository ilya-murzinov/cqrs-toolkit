package example;

import core.repository.DbConfig;
import core.rest.CorsFilter;
import lombok.extern.slf4j.Slf4j;
import spark.Spark;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;

import static core.Utils.optional;
import static example.Bootstrap.bootstrap;
import static java.lang.Runtime.getRuntime;
import static java.lang.System.getenv;
import static java.time.Instant.now;

@Slf4j
public class TodoApplication {

    public static void main(String[] args) throws Exception {
        log.info("Starting {}", TodoApplication.class.getSimpleName());
        final Instant start = now();

        Spark.port(Integer.valueOf(optional(getenv("PORT")).orElse("8080")));
        CorsFilter.enableCORS();
        getRuntime().addShutdownHook(new Thread(Spark::stop));

        final String externalUrl = optional(getenv("EXTERNAL_URL")).orElse("http://localhost:8080/todos");
        final URI connectionString =
                new URI(optional(getenv("DATABASE_URL"))
                        .orElse("postgresql://test:test@localhost:5432/test"));

        final String username = connectionString.getUserInfo().split(":")[0];
        final String password = connectionString.getUserInfo().split(":")[1];
        final String dbUrl =
                "jdbc:postgresql://" +
                        connectionString.getHost() +
                        ':' +
                        connectionString.getPort() +
                        connectionString.getPath();

        final DbConfig dbConfig = new DbConfig(dbUrl, username, password);

        bootstrap(new Config(externalUrl, dbConfig));
        Spark.awaitInitialization();

        final long startupTime = Duration.between(start, now()).toMillis();
        log.info("{} started in {} ms", TodoApplication.class.getSimpleName(), startupTime);
    }
}
