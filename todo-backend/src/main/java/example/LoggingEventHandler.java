package example;

import core.actions.events.EventHandler;
import core.domain.events.Event;
import example.db.schema.tables.EventLog;
import example.db.schema.tables.records.EventLogRecord;
import org.jooq.ConnectionProvider;
import org.jooq.DSLContext;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;

import static core.rest.json.Json.toJson;
import static example.db.schema.tables.EventLog.EVENT_LOG;
import static java.util.UUID.randomUUID;
import static org.jooq.SQLDialect.POSTGRES;
import static org.jooq.conf.RenderNameStyle.LOWER;

public class LoggingEventHandler implements EventHandler {

    protected final DSLContext db;
    private final EventLog eventLog = EVENT_LOG;

    private static final Settings SETTINGS = new Settings();

    static {
        SETTINGS.setRenderNameStyle(LOWER);
        SETTINGS.setRenderSchema(false);
        SETTINGS.setExecuteLogging(false);
        SETTINGS.setAttachRecords(false);
        SETTINGS.setUpdatablePrimaryKeys(false);
    }

    public LoggingEventHandler(ConnectionProvider connectionProvider) {
        this.db = DSL.using(new DefaultConfiguration()
                .set(POSTGRES)
                .set(connectionProvider)
                .set(SETTINGS));
    }

    @Override
    public void handle(Event<?> event) {
        db.insertInto(eventLog)
                .set(new EventLogRecord(
                        randomUUID(),
                        event.modelId.value,
                        toJson(event),
                        event.eventDate,
                        event.getClass().getSimpleName()
                ))
                .execute();
    }
}
