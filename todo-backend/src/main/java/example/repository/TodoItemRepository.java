package example.repository;

import core.repository.ModelRepository;
import example.db.schema.tables.records.TodoItemRecord;
import example.domain.TodoItem;
import example.domain.TodoItem.TodoItemState;
import org.jooq.ConnectionProvider;

import java.util.List;

import static core.Utils.optional;
import static example.db.schema.tables.TodoItem.TODO_ITEM;
import static example.domain.TodoItem.Builder.existingTodoItem;
import static example.domain.TodoItem.TodoItemState.ACTIVE;

public class TodoItemRepository extends ModelRepository<TodoItem, TodoItemRecord> {

    public TodoItemRepository(ConnectionProvider connectionProvider) {
        super(TodoItemRecord.class, TodoItem.class, TODO_ITEM, connectionProvider);
    }

    public List<TodoItem> findAll() {
        return findAllWhere(notDeleted());
    }

    public List<TodoItem> findActive() {
        return findAllWhere(TODO_ITEM.STATE.eq(ACTIVE.name()));
    }

    @Override
    protected TodoItem fromRecord(TodoItemRecord record) {
        return existingTodoItem(record.getId(), TodoItemState.valueOf(record.getState()))
                .title(record.getTitle())
                .order(record.getItemOrder())
                .description(optional(record.getDescription()))
                .created(record.getCreatedDate())
                .updated(record.getUpdatedDate())
                .build();
    }

    @Override
    protected TodoItemRecord toRecord(TodoItem model) {
        return new TodoItemRecord(
                model.id,
                model.state.name(),
                model.title,
                model.description.orElse(null),
                model.created,
                model.updated,
                model.order
        );
    }
}
