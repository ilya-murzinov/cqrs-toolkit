package example.domain;

import core.domain.Model;
import core.domain.ModelBuilder;
import core.domain.State;
import example.events.TodoItemDescriptionChangedEvent;
import example.events.TodoItemOrderChangedEvent;
import example.events.TodoItemTitleChangedEvent;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Optional;
import java.util.UUID;

import static core.Utils.none;
import static core.Utils.some;
import static example.domain.TodoItem.TodoItemState.ACTIVE;
import static example.domain.TodoItem.TodoItemState.DONE;
import static example.domain.TodoItem.TodoItemState.INACTIVE;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TodoItem extends Model<TodoItem, TodoItem.TodoItemState, TodoItem.Builder> {

    public final String title;
    public final int order;
    public final Optional<String> description;

    private TodoItem(Builder builder) {
        super(builder);
        this.title = builder.title;
        this.order = builder.order;
        this.description = builder.description;
    }

    public TodoItem title(String title) {
        if (this.title.equals(title))
            return this;

        return copy()
                .title(title)
                .addEvent(new TodoItemTitleChangedEvent(this.id(), this.title, title))
                .build();
    }

    public TodoItem order(int order) {
        if (this.order == order)
            return this;

        return copy()
                .order(order)
                .addEvent(new TodoItemOrderChangedEvent(this.id(), this.order, order))
                .build();
    }

    public TodoItem description(String description) {
        if (this.description.map(d -> d.equals(description)).orElse(false))
            return this;

        return copy()
                .description(description)
                .addEvent(new TodoItemDescriptionChangedEvent(this.id(), this.description, description))
                .build();
    }

    public TodoItem activate() {
        checkStateIs(INACTIVE);
        return withState(ACTIVE);
    }

    public TodoItem deactivate() {
        checkStateIs(ACTIVE);
        return withState(INACTIVE);
    }

    public TodoItem done() {
        checkStateIs(ACTIVE);
        return withState(DONE);
    }

    @Override
    protected Builder copy() {
        return new Builder()
                .baseCopyOf(this)
                .title(this.title)
                .order(this.order)
                .description(this.description);
    }

    public static class Builder extends ModelBuilder<TodoItem, TodoItemState, Builder> {

        private String title;
        private int order;
        private Optional<String> description = none();

        private Builder() {

        }

        public static Builder newTodoItem() {
            return new Builder().newModel().state(ACTIVE);
        }

        public static Builder existingTodoItem(UUID id, TodoItemState state) {
            return new Builder().existing(id).state(state);

        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder order(int order) {
            this.order = order;
            return this;
        }

        public Builder description(Optional<String> description) {
            this.description = description;
            return this;
        }

        public Builder description(String description) {
            this.description = some(description);
            return this;
        }

        @Override
        public TodoItem build() {
            return new TodoItem(this);
        }
    }

    public enum TodoItemState implements State {
        ACTIVE,
        INACTIVE,
        DONE
    }
}
