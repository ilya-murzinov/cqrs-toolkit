package example;

import core.actions.ActionRuntime;
import core.actions.ActionsRegistry;
import core.repository.DbConfig;
import core.repository.DomainRepository;
import core.repository.TransactionManager;
import core.rest.Resource;
import example.actions.TodoItemCompleteAction;
import example.actions.TodoItemCreateAction;
import example.actions.TodoItemDeleteAction;
import example.actions.TodoItemUpdateAction;
import example.domain.TodoItem;
import example.repository.TodoItemRepository;
import example.resources.InternalTodoResource;
import example.resources.TodoResource;
import org.flywaydb.core.Flyway;
import org.jooq.ConnectionProvider;

import java.util.List;

import static core.actions.ActionsRegistry.actionsRegistry;
import static core.repository.DomainRepository.Builder.domainRepository;
import static core.repository.SimpleConnectionProvider.simpleConnectionProvider;

public class Bootstrap {
    public final TodoItemRepository todoItemRepo;
    public final DomainRepository domainRepo;
    public final ActionRuntime actionRuntime;

    private Bootstrap(Config config) {
        final DbConfig dbConfig = config.dbConfig;
        final Flyway flyway = new Flyway();
        flyway.setDataSource(dbConfig.connectionString, dbConfig.user, dbConfig.password);
        flyway.setLocations("classpath:db.migration");
        flyway.setPlaceholderPrefix("-unused-");
        flyway.migrate();

        final ConnectionProvider cp = simpleConnectionProvider(dbConfig);

        this.todoItemRepo = new TodoItemRepository(cp);

        this.domainRepo = domainRepository()
                .addRepository(TodoItem.class, todoItemRepo)
                .build();

        final TransactionManager tx = new TransactionManager(cp);

        final ActionsRegistry registry = actionsRegistry()
                .register(TodoItemCreateAction.class, TodoItemCreateAction::new)
                .register(TodoItemUpdateAction.class, () -> new TodoItemUpdateAction(domainRepo))
                .register(TodoItemCompleteAction.class, () -> new TodoItemCompleteAction(domainRepo))
                .register(TodoItemDeleteAction.class, () -> new TodoItemDeleteAction(domainRepo));

        final LoggingEventHandler eventHandler = new LoggingEventHandler(cp);

        this.actionRuntime = new ActionRuntime(registry, domainRepo, tx, eventHandler);

        final List<Resource> resources = List.of(
                new TodoResource(todoItemRepo, actionRuntime, config.externalUrl),
                new InternalTodoResource(todoItemRepo));

        resources.forEach(Resource::registerRoutes);
    }

    public static Bootstrap bootstrap(Config config) {
        return new Bootstrap(config);
    }
}
