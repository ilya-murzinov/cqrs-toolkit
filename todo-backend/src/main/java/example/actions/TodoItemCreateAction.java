package example.actions;

import core.actions.Action;
import core.actions.Result;
import example.domain.TodoItem;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Optional;

import static core.actions.Result.ok;
import static example.domain.TodoItem.Builder.newTodoItem;

public class TodoItemCreateAction extends Action<TodoItemCreateAction.Params, TodoItem> {

    @AllArgsConstructor
    @ToString
    public static class Params {
        private final String title;
        private final int order;
        private final Optional<String> description;
    }

    @Override
    public Result<TodoItem> run(Params params) {
        final TodoItem todoItem = newTodoItem()
                .title(params.title)
                .order(params.order)
                .description(params.description)
                .build();

        batch.add(todoItem);

        return ok(todoItem);
    }
}
