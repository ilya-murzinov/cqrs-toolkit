package example.actions;

import core.actions.Action;
import core.actions.Result;
import core.domain.Id;
import core.repository.DomainRepository;
import example.domain.TodoItem;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Optional;

import static core.actions.Result.ok;

public class TodoItemCompleteAction extends Action<TodoItemCompleteAction.Params, TodoItem> {

    @AllArgsConstructor
    @ToString
    public static class Params {
        private final Id<TodoItem> id;
        private final Optional<String> title;
    }

    private final DomainRepository domainRepo;

    public TodoItemCompleteAction(DomainRepository domainRepo) {
        this.domainRepo = domainRepo;
    }

    @Override
    public Result<TodoItem> run(Params params) {
        final TodoItem todoItem = domainRepo.get(params.id);

        final TodoItem withTitle = params.title.map(todoItem::title).orElse(todoItem);
        final TodoItem done = withTitle.done();

        batch.update(done);

        return ok(done);
    }
}
