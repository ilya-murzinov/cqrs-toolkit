package example.actions;

import core.actions.Action;
import core.actions.Result;
import core.domain.Id;
import core.repository.DomainRepository;
import example.domain.TodoItem;
import lombok.AllArgsConstructor;
import lombok.ToString;

import static core.actions.Result.ok;

public class TodoItemDeleteAction extends Action<TodoItemDeleteAction.Params, TodoItem> {

    @AllArgsConstructor
    @ToString
    public static class Params {
        private final Id<TodoItem> id;
    }

    private final DomainRepository domainRepo;

    public TodoItemDeleteAction(DomainRepository domainRepo) {
        this.domainRepo = domainRepo;
    }

    @Override
    public Result<TodoItem> run(Params params) {
        final TodoItem todoItem = domainRepo.get(params.id);

        batch.delete(todoItem);

        return ok(todoItem);
    }
}
