package example.actions;

import core.actions.Action;
import core.actions.Result;
import core.domain.Id;
import core.repository.DomainRepository;
import example.domain.TodoItem;
import lombok.AllArgsConstructor;
import lombok.ToString;

import java.util.Optional;

import static core.actions.Result.ok;

public class TodoItemUpdateAction extends Action<TodoItemUpdateAction.Params, TodoItem> {

    @AllArgsConstructor
    @ToString
    public static class Params {
        private final Id<TodoItem> id;
        private final Optional<String> title;
        private final Optional<Integer> order;
        private final Optional<String> description;
    }

    private final DomainRepository domainRepo;

    public TodoItemUpdateAction(DomainRepository domainRepo) {
        this.domainRepo = domainRepo;
    }

    @Override
    public Result<TodoItem> run(Params params) {
        final TodoItem todoItem = domainRepo.get(params.id);

        final TodoItem withTitle = params.title.map(todoItem::title).orElse(todoItem);
        final TodoItem withOrder = params.order.map(todoItem::order).orElse(withTitle);
        final TodoItem updated = params.description.map(todoItem::description).orElse(withOrder);

        batch.update(updated);

        return ok(updated);
    }
}
