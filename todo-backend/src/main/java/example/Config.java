package example;

import core.repository.DbConfig;

public class Config {
    public final String externalUrl;
    public final DbConfig dbConfig;

    public Config(String externalUrl, DbConfig dbConfig) {
        this.externalUrl = externalUrl;
        this.dbConfig = dbConfig;
    }
}
