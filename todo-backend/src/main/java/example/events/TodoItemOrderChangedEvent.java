package example.events;

import core.domain.Id;
import core.domain.events.Event;
import example.domain.TodoItem;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TodoItemOrderChangedEvent extends Event<TodoItem> {

    private final int oldOrder;
    private final int order;

    public TodoItemOrderChangedEvent(
            Id<TodoItem> modelId,
            int oldOrder,
            int order) {
        super(modelId);
        this.oldOrder = oldOrder;
        this.order = order;
    }
}
