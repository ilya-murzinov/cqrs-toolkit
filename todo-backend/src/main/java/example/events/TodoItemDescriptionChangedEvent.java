package example.events;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import example.domain.TodoItem;
import core.domain.Id;
import core.domain.events.Event;

import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TodoItemDescriptionChangedEvent extends Event<TodoItem> {

    private final Optional<String> oldDescription;
    private final String description;

    public TodoItemDescriptionChangedEvent(
            Id<TodoItem> modelId,
            Optional<String> oldDescription,
            String description) {
        super(modelId);
        this.oldDescription = oldDescription;
        this.description = description;
    }
}
