package example.events;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import example.domain.TodoItem;
import core.domain.Id;
import core.domain.events.Event;

@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class TodoItemTitleChangedEvent extends Event<TodoItem> {

    private final String oldTitle;
    private final String title;

    public TodoItemTitleChangedEvent(
            Id<TodoItem> modelId,
            String oldTitle,
            String title) {
        super(modelId);
        this.oldTitle = oldTitle;
        this.title = title;
    }
}
