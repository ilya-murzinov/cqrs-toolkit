package example.functional;

import core.domain.Model;
import example.Config;
import io.restassured.specification.RequestSpecification;
import org.testcontainers.containers.PostgreSQLContainer;
import core.actions.ActionRuntime;
import core.domain.ModelBuilder;
import core.domain.Id;
import example.Bootstrap;
import core.repository.DbConfig;
import core.repository.DomainRepository;
import spark.Spark;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static example.Bootstrap.bootstrap;

public abstract class FunctionalTestSupport {

    protected static final Bootstrap bootstrap;
    protected static final DomainRepository domainRepo;
    protected static final ActionRuntime actionRuntime;
    protected static final RequestSpecification client;

    static {
        final PostgreSQLContainer container = new PostgreSQLContainer();

        container.start();

        final DbConfig dbConfig = new DbConfig(
                container.getJdbcUrl(),
                container.getUsername(),
                container.getPassword());

        final int port = 4567;
        Spark.port(port);
        bootstrap = bootstrap(new Config("", dbConfig));
        Spark.awaitInitialization();

        domainRepo = bootstrap.domainRepo;
        actionRuntime = bootstrap.actionRuntime;

        client = given()
                .contentType(JSON)
                .baseUri("http://localhost/")
                .port(port);
    }

    protected <M extends Model<M, ?, ?>, B extends ModelBuilder<M, ?, ?>> M givenExists(B builder) {
        return domainRepo.add(builder.build());
    }

    protected <M extends Model<M, ?, ?>> M givenExists(M model) {
        return domainRepo.add(model);
    }

    protected <M extends Model<?, ?, ?>> void delete(M model) {
        delete(model.id());
    }

    protected <M extends Model<?, ?, ?>> void delete(Id<M> id) {
        domainRepo.delete(id);
    }
}
