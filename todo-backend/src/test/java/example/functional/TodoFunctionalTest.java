package example.functional;

import core.actions.Result;
import example.actions.TodoItemCreateAction;
import example.actions.TodoItemUpdateAction;
import example.domain.TodoItem;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static core.Utils.none;
import static core.Utils.some;
import static example.domain.TodoItem.Builder.newTodoItem;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

class TodoFunctionalTest extends FunctionalTestSupport {

    @Test
    void should_create() {
        final Result<TodoItem> result = actionRuntime.executeRetriable(
                TodoItemCreateAction.class, new TodoItemCreateAction.Params("foo", 1, none()));
        final TodoItem added = result.get();
        final TodoItem fromRepo = domainRepo.get(added.id());
        assertThat(added).isEqualTo(fromRepo);
        delete(added);
    }

    @Test
    void should_update() {
        final TodoItem todoItem = newTodoItem().title("foo").build();
        domainRepo.add(todoItem);

        final Result<TodoItem> result = actionRuntime.executeRetriable(
                TodoItemUpdateAction.class,
                new TodoItemUpdateAction.Params(todoItem.id(), some("bar"), none(), none()));

        final TodoItem updated = result.get();
        final TodoItem fromRepo = domainRepo.get(updated.id());

        assertThat(updated).isEqualTo(fromRepo);

        delete(todoItem);
    }

    @Test
    void should_update_concurrently() throws Exception {
        final TodoItem todoItem = newTodoItem().title("foo").build();
        domainRepo.add(todoItem);

        final ExecutorService executor = Executors.newFixedThreadPool(2);

        executor.submit(() -> actionRuntime.executeRetriable(
                TodoItemUpdateAction.class,
                new TodoItemUpdateAction.Params(todoItem.id(), some("bar1"), none(), none())));

        executor.submit(() -> actionRuntime.executeRetriable(
                TodoItemUpdateAction.class,
                new TodoItemUpdateAction.Params(todoItem.id(), none(), none(), some("bar2"))));

        executor.shutdown();
        executor.awaitTermination(1, SECONDS);

        final TodoItem fromRepo = domainRepo.get(todoItem.id());

        assertThat(fromRepo.title).isEqualTo("bar1");
        assertThat(fromRepo.description).isEqualTo(some("bar2"));

        delete(todoItem);
    }
}
