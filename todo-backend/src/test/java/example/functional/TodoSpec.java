package example.functional;

import example.domain.TodoItem;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static core.rest.json.Json.object;
import static example.domain.TodoItem.Builder.newTodoItem;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

class TodoSpec extends FunctionalTestSupport {

    @Test
    void should_return_todos() {
        givenExists(newTodoItem().title("foo"));

        client.basePath("todos")
                .when()
                .get("/")
                .then()
                .log()
                .body()
                .statusCode(200)
                .body("$", hasSize(1))
                .body("completed[0]", equalTo(false))
                .body("title[0]", equalTo("foo"));
    }

    @Test
    void should_return_todo() {
        final TodoItem todoItem = givenExists(newTodoItem().title("foo"));

        client.basePath("todos")
                .when()
                .get(todoItem.id.toString())
                .then()
                .log()
                .body()
                .statusCode(200)
                .body("id", equalTo(todoItem.id.toString()))
                .body("completed", equalTo(false))
                .body("title", equalTo("foo"));

        delete(todoItem);
    }

    @Test
    void should_create_todo_item() {
        final String title = UUID.randomUUID().toString();

        client.basePath("todos")
                .when()
                .body(object()
                        .put("title", title)
                        .toString())
                .post("/")
                .then()
                .log()
                .body()
                .statusCode(200);

        final Optional<TodoItem> created =
                bootstrap.todoItemRepo.findActive()
                    .stream()
                    .filter(i -> i.title.equals(title))
                    .findFirst();

        assertThat(created).isPresent();

        created.ifPresent(i -> delete(i.id()));
    }
}
