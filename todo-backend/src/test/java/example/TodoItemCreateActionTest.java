package example;

import org.junit.jupiter.api.Test;
import core.actions.Result;
import example.domain.TodoItem;
import example.actions.TodoItemCreateAction;
import example.actions.TodoItemCreateAction.Params;
import core.domain.events.ModelCreatedEvent;

import static core.Utils.none;
import static core.test.BatchAssertions.assertThat;
import static core.test.FluentAssertion.that;
import static core.test.ResultAssertion.assertThat;

class TodoItemCreateActionTest {

    private final TodoItemCreateAction action = new TodoItemCreateAction();

    @Test
    void should_create_new_todo_item() {
        // given
        final String title = "title";

        // when
        final Result<TodoItem> result = action.run(new Params(title, 1, none()));

        // then
        assertThat(result).isSuccess();

        final TodoItem todoItem = result.get();

        assertThat(action.batch)
                .adds(TodoItem.class, e -> that("is correct", e.equals(todoItem)))
                .adds(todoItem.id(), e -> that("is correct", e.equals(todoItem)))
                .hasEvent(
                        ModelCreatedEvent.class,
                        todoItem.id(),
                        e -> that("has Model", e.model.equals(todoItem)))
                .hasEvent(
                        ModelCreatedEvent.class,
                        TodoItem.class,
                        e -> that("has Model", e.model.equals(todoItem)))
                .only();
    }
}
