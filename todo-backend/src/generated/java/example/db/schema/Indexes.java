/*
 * This file is generated by jOOQ.
 */
package example.db.schema;


import example.db.schema.tables.EventLog;
import example.db.schema.tables.SchemaVersion;
import example.db.schema.tables.TodoItem;

import javax.annotation.Generated;

import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code>public</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.2",
        "schema version:4"
    },
    date = "2019-04-27T23:50:58.084Z",
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index EVENT_LOG_PKEY = Indexes0.EVENT_LOG_PKEY;
    public static final Index SCHEMA_VERSION_IR_IDX = Indexes0.SCHEMA_VERSION_IR_IDX;
    public static final Index SCHEMA_VERSION_PK = Indexes0.SCHEMA_VERSION_PK;
    public static final Index SCHEMA_VERSION_S_IDX = Indexes0.SCHEMA_VERSION_S_IDX;
    public static final Index SCHEMA_VERSION_VR_IDX = Indexes0.SCHEMA_VERSION_VR_IDX;
    public static final Index TODO_ITEM_PKEY = Indexes0.TODO_ITEM_PKEY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index EVENT_LOG_PKEY = Internal.createIndex("event_log_pkey", EventLog.EVENT_LOG, new OrderField[] { EventLog.EVENT_LOG.ID }, true);
        public static Index SCHEMA_VERSION_IR_IDX = Internal.createIndex("schema_version_ir_idx", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.INSTALLED_RANK }, false);
        public static Index SCHEMA_VERSION_PK = Internal.createIndex("schema_version_pk", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.VERSION }, true);
        public static Index SCHEMA_VERSION_S_IDX = Internal.createIndex("schema_version_s_idx", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.SUCCESS }, false);
        public static Index SCHEMA_VERSION_VR_IDX = Internal.createIndex("schema_version_vr_idx", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.VERSION_RANK }, false);
        public static Index TODO_ITEM_PKEY = Internal.createIndex("todo_item_pkey", TodoItem.TODO_ITEM, new OrderField[] { TodoItem.TODO_ITEM.ID }, true);
    }
}
