/*
 * This file is generated by jOOQ.
 */
package example.db.schema.tables;


import core.repository.converters.InstantConverter;

import example.db.schema.Indexes;
import example.db.schema.Keys;
import example.db.schema.Public;
import example.db.schema.tables.records.TodoItemRecord;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.2",
        "schema version:4"
    },
    date = "2019-04-27T23:50:58.084Z",
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TodoItem extends TableImpl<TodoItemRecord> {

    private static final long serialVersionUID = -1542392645;

    /**
     * The reference instance of <code>public.todo_item</code>
     */
    public static final TodoItem TODO_ITEM = new TodoItem();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<TodoItemRecord> getRecordType() {
        return TodoItemRecord.class;
    }

    /**
     * The column <code>public.todo_item.id</code>.
     */
    public final TableField<TodoItemRecord, UUID> ID = createField("id", org.jooq.impl.SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>public.todo_item.state</code>.
     */
    public final TableField<TodoItemRecord, String> STATE = createField("state", org.jooq.impl.SQLDataType.VARCHAR(25).nullable(false), this, "");

    /**
     * The column <code>public.todo_item.title</code>.
     */
    public final TableField<TodoItemRecord, String> TITLE = createField("title", org.jooq.impl.SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>public.todo_item.description</code>.
     */
    public final TableField<TodoItemRecord, String> DESCRIPTION = createField("description", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>public.todo_item.created_date</code>.
     */
    public final TableField<TodoItemRecord, Instant> CREATED_DATE = createField("created_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new InstantConverter());

    /**
     * The column <code>public.todo_item.updated_date</code>.
     */
    public final TableField<TodoItemRecord, Instant> UPDATED_DATE = createField("updated_date", org.jooq.impl.SQLDataType.TIMESTAMP.nullable(false), this, "", new InstantConverter());

    /**
     * The column <code>public.todo_item.item_order</code>.
     */
    public final TableField<TodoItemRecord, Integer> ITEM_ORDER = createField("item_order", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>public.todo_item</code> table reference
     */
    public TodoItem() {
        this(DSL.name("todo_item"), null);
    }

    /**
     * Create an aliased <code>public.todo_item</code> table reference
     */
    public TodoItem(String alias) {
        this(DSL.name(alias), TODO_ITEM);
    }

    /**
     * Create an aliased <code>public.todo_item</code> table reference
     */
    public TodoItem(Name alias) {
        this(alias, TODO_ITEM);
    }

    private TodoItem(Name alias, Table<TodoItemRecord> aliased) {
        this(alias, aliased, null);
    }

    private TodoItem(Name alias, Table<TodoItemRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> TodoItem(Table<O> child, ForeignKey<O, TodoItemRecord> key) {
        super(child, key, TODO_ITEM);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Public.PUBLIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.TODO_ITEM_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<TodoItemRecord> getPrimaryKey() {
        return Keys.TODO_ITEM_PKEY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<TodoItemRecord>> getKeys() {
        return Arrays.<UniqueKey<TodoItemRecord>>asList(Keys.TODO_ITEM_PKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TodoItem as(String alias) {
        return new TodoItem(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TodoItem as(Name alias) {
        return new TodoItem(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public TodoItem rename(String name) {
        return new TodoItem(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public TodoItem rename(Name name) {
        return new TodoItem(name, null);
    }
}
