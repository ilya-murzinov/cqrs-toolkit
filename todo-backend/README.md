# todobackend implementation

API root: [https://cqrs-todo-backend.herokuapp.com/todos/](https://cqrs-todo-backend.herokuapp.com/todos/)

API specs: [http://www.todobackend.com/specs/index.html?https://cqrs-todo-backend.herokuapp.com/todos/](http://www.todobackend.com/specs/index.html?https://cqrs-todo-backend.herokuapp.com/todos/)